#!/bin/bash
#
# dyndnsmadeeasy-install.sh
#
# This script installs the script and crontab.
#
ERR=0
for f in dyndnsmadeeasy.crontab dyndnsmadeeasy-update.sh ; do
	[[ -f $f ]] && continue
	ERR=1
	echo "Missing $f from distribution"
done
(( ERR == 1 )) && exit 1

TIMESTAMP=$(date +"%Y-%m-%d-%H%M")
[[ -d /usr/local/bin ]] || mkdir -p /usr/local/bin || exit 1
SRC=dyndnsmadeeasy-update.sh 
DST=/usr/local/bin/dyndnsmadeeasy-update.sh 
if [[ -f $DST ]]; then
	if ! diff -q $SRC $DST ; then
		/bin/mv -v $DST $DST.$TIMESTAMP
		/bin/cp -v $SRC $DST
		chmod 755 $DST
	fi
else
	/bin/cp -v $SRC $DST
	chmod 755 $DST
fi

SRC=dyndnsmadeeasy.crontab
DST=/etc/cron.d/dyndnsmadeeasy
if [[ -f $DST ]]; then
	if ! diff -q $SRC $DST ; then
		/bin/mv -v $DST $DST.$TIMESTAMP.cfsaved
		/bin/cp -v $SRC $DST
		chmod 644 $DST
	fi
else
	/bin/cp -v $SRC $DST
	chmod 644 $DST
fi
