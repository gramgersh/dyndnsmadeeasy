#!/bin/sh
#
# dyndnsmadeeasy-update.sh
#
# This script is made to limit the number of updates.  The script requires
# the 'dig' command to look up the current IP and not send an update if there
# is no change.

CFGFILE=/etc/dyndnsmadeeasy.cfg
while getopts c: c ; do
	case "$c" in
		c ) CFGFILE="$OPTARG" ;;
		* ) echo "Unknown option '$c'"
			exit 1
			;;
	esac
done

GETCMD=""
if type -p curl >/dev/null 2>/dev/null ; then
	GETCMD="curl -s"
elif type -p wget >/dev/null 2>/dev/null ; then
	GETCMD="wget -q -O -"
else
	echo "This script needs either curl or wget"
	exit 1
fi
if ! type -p dig >/dev/null 2>/dev/null ; then
	echo "This script needs dig from the bind-utils package"
	exit 1
fi

#
# Read the config file
if [[ ! -f "$CFGFILE" ]]; then
	echo "$CFGFILE: no such file"
	exit 1
fi
DMEUSER=""
DMEPASS4=""
DMEPASS6=""
DMEID4=""
DMEID6=""
DMEHOST4=""
DMEHOST6=""

exec 3<$CFGFILE
while IFS="=" read -u3 VAR VAL ; do
	case "$VAR" in
		DMEHOST4 ) DMEHOST4="$VAL" ;;
		DMEHOST6 ) DMEHOST6="$VAL" ;;
		DMEID4 ) DMEID4="$VAL" ;;
		DMEID6 ) DMEID6="$VAL" ;;
		DMEPASS4 ) DMEPASS4="$VAL" ;;
		DMEPASS6 ) DMEPASS6="$VAL" ;;
		DMEUSER ) DMEUSER="$VAL" ;;
	esac
done

if [[ -z "$DMEUSER" ]]; then
	echo "Cannot find DMEUSER in $CFGFILE"
	exit 1
fi

BASEURL="https://cp.dnsmadeeasy.com/servlet/updateip?username=$DMEUSER"
XVAL=0

if [[ -n "$DMEPASS4" && -n "$DMEID4" && -n "$DMEHOST4" ]]; then
	CURIP=$($GETCMD -4 https://icanhazip.com)
	CURDNS=$(dig -t A +short $DMEHOST4)
	if [[ "$CURIP" != "$CURDNS" ]]; then
		RESULTS=$($GETCMD "${BASEURL}&password=$DMEPASS4&id=$DMEID4&ip=$CURIP" 2>&1)
		case "$RESULTS" in
			success )
			    logger -t 'Dyn-DNS-Made-Easy' -s "DNS record for $DMEHOST4 updated from $CURDNS to $CURIP"
				echo 'Dyn-DNS-Made-Easy' -s "DNS record for $DMEHOST4 updated from $CURDNS to $CURIP"
				;;
			* )
			    echo "ERROR updating DNS record for $DMEHOST4 from $CURDNS to $CURIP:  $RESULTS"
				XVAL=1
				;;
		esac
	fi
fi

if [[ -n "$DMEPASS6" && -n "$DMEID6" && -n "$DMEHOST6" ]]; then
	CURIP=$($GETCMD -6 https://icanhazip.com)
	CURDNS=$(dig -t AAAA +short $DMEHOST6)
	RESULTS=$($GETCMD "${BASEURL}&password=$DMEPASS6&id=$DMEID6&ip=$CURIP" 2>&1)
	case "$RESULTS" in
		success )
			logger -t 'Dyn-DNS-Made-Easy' -s "DNS record for $DMEHOST6 updated from $CURDNS to $CURIP"
			echo 'Dyn-DNS-Made-Easy' -s "DNS record for $DMEHOST6 updated from $CURDNS to $CURIP"
			;;
		* )
		    echo "ERROR updating DNS record for $DMEHOST6 from $CURDNS to $CURIP:  $RESULTS"
			XVAL=1
			;;
	esac
fi

exit $XVAL
